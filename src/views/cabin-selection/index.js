import Header from "../../components/Header/HeaderAuth";
import BookingFlowCards from "../../components/Cards/BookingFlowCards";
import Footer from "../../components/Footer/Footer";
import Container from "../../components/Container";
import {Button } from "antd";
import CardBg from "../../assets/img/cabin-selection/card-bg.png"

function RoomSelection() {
  return (
    <div className="Home">
      <Header className="mb-12" />
      <BookingFlowCards.ProgressBar />
      <Container>
        <h4 className="text-xl font-medium">Add Cabin & Guests</h4>
        <div className="grid grid-cols-2 md:grid-cols-4 gap-4 items-center my-5">
          <div className="p-5 rounded-lg shadow-lg">
            <div className="mb-2">
              <span className="text-lg font-medium">Cabin 1</span>{" "}
              <i class="fas fa-trash-alt text-right float-right text-j-orange"></i>
            </div>
            <p>Guests: 02</p>
          </div>
          <div className="p-5 rounded-lg shadow-lg">
            <div className="mb-2">
              <span className="text-lg font-medium">Cabin 2</span>{" "}
              <i class="fas fa-trash-alt text-right float-right text-j-orange"></i>
            </div>
            <p>Guests: 02</p>
          </div>
          <div className="p-5 rounded-lg shadow-lg border border-j-orange">
            <div className="mb-2">
              <span className="text-lg font-medium">Cabin 3</span>{" "}
              <i class="fas fa-trash-alt text-right float-right text-j-orange"></i>
            </div>
            <p>Guests: 02</p>
          </div>
          <div className="p-5">
            <Button>Add Cabin</Button>
          </div>
        </div>
        <div className="grid grid-cols-1 lg:grid-cols-3 gap-4 items-center my-5">
        <div className="p-5 rounded-lg shadow-lg mb-12">
            <p className="text-lg font-medium mb-2">Cabin 3</p>
            <p className="italic text-j-gray"><i class="fas fa-couch mr-3"></i>Cabins can accomodate 2,3 or 4 Guests</p>
            <div className="grid grid-cols-6 gap-4 my-5">
              <div className="col-span-1  w-12 h-12 bg-j-gray-lighter rounded text-white text-center"><i class="fas fa-minus py-4"></i></div>
              <div className="col-span-4 text-center">
                <div>1</div>
                <div>Adult</div>
                <div className="text-j-gray-light text-xs">12 & Above</div>
              </div>
              <div className="col-span-1  w-12 h-12 bg-j-orange rounded text-white text-center"><i class="fas fa-plus py-4"></i></div>
            </div>
            <div className="grid grid-cols-6 gap-4 my-5">
              <div className="col-span-1  w-12 h-12 bg-j-gray-lighter rounded text-white text-center"><i class="fas fa-minus py-4"></i></div>
              <div className="col-span-4 text-center">
                <div>0</div>
                <div>Children</div>
                <div className="text-j-gray-light text-xs">2 Years - 12 Years</div>
              </div>
              <div className="col-span-1  w-12 h-12 bg-j-orange rounded text-white text-center"><i class="fas fa-plus py-4"></i></div>
            </div>
            <div className="grid grid-cols-6 gap-4 my-5">
              <div className="col-span-1  w-12 h-12 bg-j-gray-lighter rounded text-white text-center"><i class="fas fa-minus py-4"></i></div>
              <div className="col-span-4 text-center">
                <div>0</div>
                <div>Infants</div>
                <div className="text-j-gray-light text-xs">6 Month - 2 Years</div>
              </div>
              <div className="col-span-1  w-12 h-12 bg-j-orange rounded text-white text-center"><i class="fas fa-plus py-4"></i></div>
            </div>
            <p className="italic text-j-gray">* Infants below 6 months are not allowed to travel</p>
          </div>
          <div className="p-12 lg:h-full rounded-lg shadow-lg col-span-2" style={{background:`url(${CardBg})`,backgroundSize:"cover"}}>
            <h3 className="text-white text-xl font-bold">Book now to get benefits on May - June 2021 sailings:</h3>
            <ul className="text-white pl-5 mb-8">
              <li>&bull; Book with just Rs 500</li>
              <li>&bull; Free Cancellation</li>
              <li>&bull; Free Rescheduling up to 4 days before sailing date</li>
              <li>&bull; Free sailing for kids*</li>
            </ul>
            <p className="italic text-white">Terms and conditions apply*</p>
          </div>
        </div>
      </Container>
      <Footer />
    </div>
  );
}

export default RoomSelection;
