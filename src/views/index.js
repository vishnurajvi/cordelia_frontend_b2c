import React, { Component } from "react";
import "./index.scss";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Home from "./home/index"
import CruiseRoutes from "./cruise-routes/index"
import RoomSelection from "./room-selection/index"
import CabinSelection from "./cabin-selection/index"
import DeckSelection from "./deck-selection/index"
import BookingSummary from "./booking-summary/index"
import GuestDetails from "./guest-details/index"
import ShoreExcursions from "./shore-excursions/index"
import PaymentDetails from "./payment-details/index"
import ConfirmedBookingDetails from "./confirmed-booking-details/index"
import My404Component from "./404"

class App extends Component {
  render() {
    return (
    <Router>
          <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/cruise-routes' component={CruiseRoutes} />
              <Route path='/room-selection' component={RoomSelection} />
              <Route path='/cabin-selection' component={CabinSelection} />
              <Route path='/deck-selection' component={DeckSelection} />
              <Route path='/booking-summary' component={BookingSummary} />
              <Route path='/guest-details' component={GuestDetails} />
              <Route path='/shore-excursions' component={ShoreExcursions} />
              <Route path='/payment-details' component={PaymentDetails} />
              <Route path='/confirmed-booking-details' component={ConfirmedBookingDetails} />
              <Route path='/404' component={My404Component} />
              <Redirect from='*' to='/404' />
          </Switch>
      </Router>
    );
  }
}

export default App;
