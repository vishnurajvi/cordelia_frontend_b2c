import NotificationBanner from "../../components/NotificationBanner/NotificationBanner";
import Header from "../../components/Header/HeaderAuth";
import SearchComponent from "../../components/SearchComponent/SearchComponent";
import Footer from "../../components/Footer/Footer";
import HomePageCards from "../../components/Cards/HomePageCards";

function Home() {
  return (
    <div className="Home">
      <NotificationBanner />
      <Header />
      <SearchComponent />
      <HomePageCards.MovingTextCard />
      <HomePageCards />
      <HomePageCards.TagCards />
      <HomePageCards.NewsLetterCard />
      <HomePageCards.BannerCarouselBackground />
      <HomePageCards.ImageTextOverlayCards />
      <HomePageCards.FeaturesCards />
      <HomePageCards.TopEverythingComponent />
      <Footer />
    </div>
  );
}

export default Home;
