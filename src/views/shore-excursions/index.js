import Header from "../../components/Header/HeaderAuth";
import Footer from "../../components/Footer/Footer";
import Card from "../../components/UI/Card";
import "./shore.scss";
import CardbgImg from "../../assets/img/bg-package-card.png";
import Slider1 from "../../assets/img/imageSlide1.jpg";
import Beach from "../../assets/img/beach.jpg";
import Packge1 from "../../assets/img/packge1.jpg";
import Packge2 from "../../assets/img/packge2.jpg";
import Packge3 from "../../assets/img/packge3.jpg";
import Container from "../../components/Container";

function Home() {
  return (
    <>
      <Header />
      <div className="packag--save--section">
        <Container>
          <div className="grid grid-cols-1 lg:grid-cols-3 gap-4 mt-20 my-5">
            <Card className="cardpackage--bg lg:col-span-2">
              <div
                className="full--bgcard-view"
                style={{ backgroundImage: `url(${CardbgImg})` }}
              >
                <div className="package--choose-txt">
                  <h3>Choose PACKAGES and Save upto 25%</h3>
                  <p>
                    Amet minim mollit non test ullamco est sit aliqua dolor do
                    amet sint. Velit officia consequat duis enim velit mollit.
                    Exercitation veniam consequat sunt nostrud amet.1
                  </p>
                </div>
              </div>
            </Card>
            <Card>
              <div className="card-body p-5">
                <div className="cardtable--bookitems">
                  <h3 className="pricedtl--title">Price Details</h3>
                  <table class="table tableprice--dtl">
                    <tbody>
                      <tr className="tr-bookview">
                        <td>Total Booking Amount</td>
                        <td>₹55,123.00</td>
                      </tr>
                      <tr className="tr-bookview">
                        <td className="text-success">Discount</td>
                        <td className="text-success">-₹2,123.00</td>
                      </tr>
                      <tr className="tr-bookview">
                        <td className="total-amt--book pt-2">
                          Total Payable Amount
                        </td>
                        <td className="extra--book-td pt-2">₹56,246</td>
                      </tr>
                    </tbody>
                  </table>
                  <div className="text-right">
                    <button className="btn btn-proceed">PROCEED</button>
                  </div>
                </div>
              </div>
            </Card>
          </div>
        </Container>
      </div>
      <div className="tab-section--wraper pb-5">
        <Container>
          <div className="main-head-view pb-3">
            <h3 className="heading-h3">
              EXPLORE <strong>ACTIVITIES</strong>
            </h3>
          </div>
          <ul className="nav-tab-pills mb-5">
            <li className="nav-items--main active mb-2">
              <a className="navLink--tab" href="#">
                LAKSHADWEEP
              </a>
            </li>
            <li className="nav-items--main mb-2">
              <a className="navLink--tab" href="#">
                COCHIN
              </a>
            </li>
            <li className="nav-items--main mb-2">
              <a className="navLink--tab" href="#">
                GOA
              </a>
            </li>
          </ul>
          <h4 className="subtab--title">ACTIVITIES IN GOA</h4>
          <div className="grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-12 shadow-xl rounded-lg my-5">
            <div className="lg:col-span-1 xl:col-span-5">
              <img src={Slider1} alt="gg" />
            </div>
            <div className="lg:col-span-1 xl:col-span-7 p-8">
              <div className="topHead--excurr">
                <h4>Inclusion: -</h4>
                <p>
                  Transfers, Entrances, Bottle of water, Excursion Insurance &
                  local guide charges
                </p>
              </div>
              <div className="disc-exccur-txt">
                <p>
                  Leave pier & drive through Commercial districts Pettah & Fort-
                  Continue the drive passing old parliament, St. Lucia church,
                  mosque, Hindu temple & Gangarama temple on to Town Hall- Photo
                  stop at Colombo town hall & Victoria park -Continue the drive
                  passing Colombo museum, independence square, elite residential
                  area of Cinnamon gardens (photo stops)-Visit & shopping at
                  traditional handicraft/gem/souvenir shop-Drive back to the
                  Local Restaurant -Lunch at local restaurant -Drive back to
                  pier and aboard the ship{" "}
                </p>
              </div>
              <div className="bottom-view--transf">
                <div className="leftSide--tranitem">
                  <ul className="mb-0">
                    <li>
                      Duration: <span>4HRS</span>
                    </li>
                    <li className="mb-0">
                      Transfered By: <span>SIC Tour (Coach)</span>
                    </li>
                  </ul>
                </div>
                <div className="rightRemove--btn">
                  <button type="text" className="btn btn-remove">
                    REMOVE
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-12 shadow-xl rounded-lg my-5">
            <div className="lg:col-span-1 xl:col-span-5">
              <img src={Beach} alt="gg" />
            </div>
            <div className="lg:col-span-1 xl:col-span-7 p-8">
              <div className="topHead--excurr">
                <h4>Inclusion: -</h4>
                <p>
                  Transfers, Entrances, Bottle of water, Excursion Insurance &
                  local guide charges
                </p>
              </div>
              <div className="disc-exccur-txt">
                <p>
                  Leave pier & drive through Commercial districts Pettah & Fort-
                  Continue the drive passing old parliament, St. Lucia church,
                  mosque, Hindu temple & Gangarama temple on to Town Hall- Photo
                  stop at Colombo town hall & Victoria park -Continue the drive
                  passing Colombo museum, independence square, elite residential
                  area of Cinnamon gardens (photo stops)-Visit & shopping at
                  traditional handicraft/gem/souvenir shop-Drive back to the
                  Local Restaurant -Lunch at local restaurant -Drive back to
                  pier and aboard the ship{" "}
                </p>
              </div>
              <div className="bottom-view--transf">
                <div className="leftSide--tranitem">
                  <ul className="mb-0">
                    <li>
                      Duration: <span>4HRS</span>
                    </li>
                    <li className="mb-0">
                      Transfered By: <span>SIC Tour (Coach)</span>
                    </li>
                  </ul>
                </div>
                <div className="rightRemove--btn">
                  <button type="text" className="btn btn-remove">
                    REMOVE
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
      <div className="package-section--wraper pb-5">
        <Container>
          <h3 className="heading-h3">
            EXPLORE <strong>SPA PACKAGES</strong>
          </h3>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
            <div className="package-col-Img">
              <img src={Packge1} />
              <div className="package--name-posit">
                <h4 className="package--title">SILVER PACKAGE</h4>
              </div>
            </div>
            <div className="package-col-Img">
              <img src={Packge2} />
              <div className="package--name-posit">
                <h4 className="package--title">GOLD PACKAGE</h4>
              </div>
            </div>
            <div className="package-col-Img">
              <img src={Packge3} />
              <div className="package--name-posit">
                <h4 className="package--title">COUPLE PACKAGE</h4>
              </div>
            </div>
          </div>
        </Container>
      </div>
      <Footer />
    </>
  );
}

export default Home;
