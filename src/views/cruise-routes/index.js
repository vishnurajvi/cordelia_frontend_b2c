import NotificationBanner from "../../components/NotificationBanner/NotificationBanner";
import Header from "../../components/Header/HeaderAuth";
import SearchComponent from "../../components/SearchComponent/SearchComponent";
import Footer from "../../components/Footer/Footer";
import CruiseRoutesCards from "../../components/Cards/CruiseRoutesCards";
import Banner from "../../components/Banner";
import Search from "../../components/Search";
import Container from "../../components/Container";

function CruiseRoutes() {
  return (
    <div className="Home">
      <NotificationBanner />
      <Header />
      <Banner/>
      <Search />
      {/* Filters */}
      <>
                <Container>
                    <div className="grid lg:grid-flow-cols grid-cols-4 lg:grid-cols-12 p-3">
                        <p className="px-3 mr-3 col-span-1 row-span-2 lg:row-span-auto"><i class="fas fa-filter mr-3 text-j-gray-light"></i>Filter</p>
                        <p className="px-3 col-span-1">2 Night</p>
                        <p className="px-3 col-span-1">3 Night</p>
                        <p className="px-3 col-span-1">4 Night</p>
                        <p className="px-3 col-span-1">5 Night</p>
                        <p className="px-3 col-span-1">6 Night</p>
                        <p className="px-3 col-span-1">7 Night</p>
                    </div>
                    <div className="p-3">
                    <p className="px-3 mr-3"><i class="fas fa-sort-amount-up mr-3 text-j-gray-light"></i>Sort</p>
                    </div>
                </Container>
      </>
      {/* <CruiseRoutesCards /> */}
      <CruiseRoutesCards.TravelItineraryCards />
      <Footer />
    </div>
  );
}

export default CruiseRoutes;
