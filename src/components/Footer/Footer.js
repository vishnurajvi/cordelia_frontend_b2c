import styles from "./Footer.module.scss";
import { Row, Col } from "antd";
import WhiteLogo from "../../assets/img/footer-logo.png";
import Container from "../Container";
import {
  FacebookFilled,
  InstagramFilled,
  LinkedinFilled,
  TwitterCircleFilled,
  MailFilled,
  PhoneFilled,
} from "@ant-design/icons";

function Footer() {
  return (
    <footer style={{ backgroundImage: "url(/images/footer-bg.png)" }}>
      <Container wrapperClass="bg-j-magenta" innerClass="p-6">
        <div className="grid grid-cols-1 lg:grid-cols-5 text-white border-b mb-6 pb-6">
          <div className="col-span-2 p-3 lg:pr-36">
            <div className="grid grid-cols-2 items-center text-right mb-2">
              <img src={WhiteLogo} alt="Site Name" />
              <div>
                <h3 className="text-2xl lg:text-3xl font-medium text-white mb-0">
                  Cordelia Cruises
                </h3>
                <h6 className="text-base text-white">A city on the sea</h6>
              </div>
            </div>
            <p className="font-light">
              Cordelia Cruises by Waterways Leisure Tourism Pvt Ltd is India’s
              premium cruise liner. True to its name, Cordelia aspires to
              promote and drive the cruise culture in India through experiences
              that are stylish, luxurious and most importantly, inherently
              Indian.
            </p>
            <p className="text-lg">
              Follow Us:
              <i class="fab fa-facebook ml-4 mx-2"></i>
              <i class="fab fa-instagram mx-2"></i> 
              <i class="fab fa-linkedin mx-2"></i>
              <i class="fab fa-twitter mx-2"></i>
            </p>
          </div>
          <div className="col-span-1 p-3 text-left">
            <p className="mb-3 text-lg">
              <strong>Quick Links</strong>
            </p>
            <p>Destination</p>
            <p>Group booking form</p>
            <p>Lost and Found</p>
            <p>Agent Login</p>
            <p>About Us</p>
            <p>Promotions</p>
            <p>FAQ</p>
          </div>
          <div className="col-span-1 p-3">
            <p className="mb-3 text-lg">
              <strong>Privacy & Policy</strong>
            </p>
            <p>Privacy policy</p>
            <p>Onboard policy</p>
            <p>Healthy waves policy</p>
            <p>Clean waves policy</p>
            <p>Terms and conditions</p>
            <p>Passenger Cruise Ticket Contract</p>
          </div>
          <div className="col-span-2 md:col-span-1 p-3">
            <p className="mb-3 text-lg">
              <strong>Contact Us</strong>
            </p>
            <p>
              <i class="fas fa-envelope mr-3"></i> info@cordeliacruises.com
            </p>
            <p>
              <i class="fas fa-phone-alt mr-3"></i> 022688111111
            </p>
          </div>
        </div>
        <div className="grid text-white">
          <p className="mb-0">
            {" "}
            &copy; 2021 Cordelia Cruises All Right Reserved
          </p>
        </div>
      </Container>
    </footer>
    // <div className="Footer">
    //   <footer className={styles.siteFooter}>
    //     <Row>
    //       <Col span={1}></Col>
    //       <Col span={6}>
    //         <div className={styles.siteFooterIdentity}>
    //           <img src={WhiteLogo} alt="Site Name" />
    //           <div>
    //             <h5>Cordelia Cruises</h5>
    //             <h6 className={styles.header6}>A city on the sea</h6>
    //           </div>
    //         </div>
    //         <p className={styles.textJustify}>
    //           Cordelia Cruises by Waterways Leisure Tourism Pvt Ltd is India’s
    //           premium cruise liner. True to its name, Cordelia aspires to
    //           promote and drive the cruise culture in India through experiences
    //           that are stylish, luxurious and most importantly, inherently
    //           Indian.
    //         </p>
    //         <ul className={styles.socialIcons}>
    //           <b>Follow Us:</b>
    //           <li>
    //             <a className={styles.facebook} href="#">
    //               <FacebookFilled />
    //             </a>
    //           </li>
    //           <li>
    //             <a className={styles.instagram} href="#">
    //               <InstagramFilled />
    //             </a>
    //           </li>
    //           <li>
    //             <a className={styles.linkedin} href="#">
    //               <LinkedinFilled />
    //             </a>
    //           </li>
    //           <li>
    //             <a className={styles.twitter} href="#">
    //               <TwitterCircleFilled />
    //             </a>
    //           </li>
    //         </ul>
    //       </Col>
    //       <Col span={1}></Col>
    //       <Col span={5}>
    //         <h6>Quick Links</h6>
    //         <ul className={styles.footerLinks}>
    //           <li>
    //             <a href="http://scanfcode.com/category/c-language/">
    //               Destination
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/front-end-development/">
    //               Group Booking Form
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/back-end-development/">
    //               Lost and Found
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/java-programming-language/">
    //               Agent Login
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/android/">About Us</a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/templates/">
    //               Promotions
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/templates/">FAQ</a>
    //           </li>
    //         </ul>
    //       </Col>
    //       <Col span={6}>
    //         <h6>Privacy & Policy</h6>
    //         <ul className={styles.footerLinks}>
    //           <li>
    //             <a href="http://scanfcode.com/category/c-language/">
    //               Privacy Policy
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/front-end-development/">
    //               Onboard Policy
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/back-end-development/">
    //               Healthy Waves Policy
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/java-programming-language/">
    //               Clean Waves Policy
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/android/">
    //               Terms and Conditions
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/templates/">
    //               Passenger Cruise Ticket Contract
    //             </a>
    //           </li>
    //         </ul>
    //       </Col>
    //       <Col span={5}>
    //         <h6>Contact Us</h6>
    //         <ul className={styles.footerLinks}>
    //           <li>
    //             <a href="http://scanfcode.com/category/c-language/">
    //               <MailFilled />{" "}
    //               <span style={{ marginLeft: "8px" }}>
    //                 info@cordeliacruises.com
    //               </span>
    //             </a>
    //           </li>
    //           <li>
    //             <a href="http://scanfcode.com/category/front-end-development/">
    //               <PhoneFilled />{" "}
    //               <span style={{ marginLeft: "8px" }}>022688111111</span>
    //             </a>
    //           </li>
    //         </ul>
    //       </Col>
    //     </Row>
    //     <Row>
    //       <Col span={1}></Col>
    //       <Col
    //         span={22}
    //         style={{ borderTop: "0.5px solid rgba(255, 255, 255, 0.5)" }}
    //       >
    //         <p className={styles.copyrightText}>
    //           &copy; 2021 Cordelia Cruises All Right Reserved
    //         </p>
    //       </Col>
    //       <Col span={1}></Col>
    //     </Row>
    //   </footer>
    // </div>
  );
}

export default Footer;
