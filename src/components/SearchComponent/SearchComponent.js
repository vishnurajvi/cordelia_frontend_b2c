import React, { useState, useEffect } from "react";
import { Form, Input, DatePicker, Button } from "antd";
import { EnvironmentFilled, CalendarFilled } from "@ant-design/icons";
import styles from "./SearchComponent.scss";

const SearchComponent = () => {
  const [form] = Form.useForm();
  const [, forceUpdate] = useState({});

  // To disable submit button at the beginning.
  useEffect(() => {
    forceUpdate({});
  }, []);

  const onFinish = (values: any) => {
    console.log("Finish:", values);
  };

  return (
    <div style={{
      position: "absolute",
      background: "white",
      left: "12.5%",
      top: "52%",
      zIndex: 10,
      textAlign: "center"
    }}>
      <Form
        form={form}
        name="horizontal_search"
        layout="inline"
        onFinish={onFinish}
        style={{
          WebkitBoxShadow: "-2px 17px 5px -7px rgba(0,0,0,0.12)",
          MozBoxShadow: "-2px 17px 5px -7px rgba(0,0,0,0.12)",
          boxShadow: "-2px 17px 5px -7px rgba(0,0,0,0.12)"
        }}
      >
        <Form.Item
          name="username"
          rules={[{ required: true, message: "Please input your Origin!" }]}
          style={{ borderRight: "1px solid #8B8B8B" }}
        >
          <Input
            suffix={<EnvironmentFilled className="site-form-item-icon" />}
            placeholder="Any Destination"
            style={{ border: "none", height: 74, width: 244, marginRight: 6 }}
          />
        </Form.Item>
        <Form.Item
          name="text"
          rules={[{ required: true, message: "Please input your Destination!" }]}
          style={{ borderRight: "1px solid #8B8B8B" }}
        >
          <Input
            suffix={<EnvironmentFilled className="site-form-item-icon" />}
            type="password"
            placeholder="Any Origin"
            style={{ border: "none", height: 74, width: 244, marginRight: 6 }}
          />
        </Form.Item>
        <Form.Item
          name="date-picker"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <DatePicker
            suffix={<CalendarFilled className="site-form-item-icon" />}
            placeholder="Month of Sail"
            style={{ border: "none", height: 74, width: 244, marginRight: 6 }}
          />
        </Form.Item>
        <Form.Item style={{ marginRight: 0 }} shouldUpdate>
          {() => (
            <Button type="primary" htmlType="submit">
              Search Cruises
            </Button>
          )}
        </Form.Item>
      </Form>
      <p style={{color: "#919191", margin: "10px auto"}}>Your Current Location is Mumbai. <a>Not in Mumbai?</a></p>
    </div>
  );
};

export default SearchComponent;
