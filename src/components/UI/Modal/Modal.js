import React, { useState, useEffect} from 'react';
import ReactDOM from "react-dom";
import cx from 'classnames';
import styles from './Modal.module.css'

const Modal = (props) => {
    const {isBackdropClickPrevented} = props;
    const onClose = e => {
        props.onClose && props.onClose(e);
    };

    if(!props.show){
        document.body.style.overflow = 'unset';
        return <></>;
    }else{
        document.body.style.overflow = 'hidden';
    }

    return (
        <>
            {ReactDOM.createPortal(
                <div className={cx(styles.modal)}  onClick={(e) => {
                    // close modal when outside of modal is clicked
                    if(!isBackdropClickPrevented){
                        onClose(e);
                    }else{
                        e.preventDefault();
                    }
                }}>
                    <div className={cx(styles.modalcontent, "rounded-xl", props.classes)} 
                        onClick={e => {
                        // do not close modal if anything inside modal content is clicked
                        e.stopPropagation();
                    }}>
                        <div>{props.children}</div>
                    </div>
                </div>
        , document.getElementById('modal-container'))}
        </>
    );
}

export default Modal;