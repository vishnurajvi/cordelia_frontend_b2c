import styles from "./NotificationBanner.module.scss";
import {
  CloseCircleOutlined
} from "@ant-design/icons";

function NotificationBanner() {
  return (
    <div className={styles.notificationBannerMain}>
      <a href="#">Covid-19 safety Updates on Cordelia</a>
      <CloseCircleOutlined style={{ color: "#500E4B"}}/>
    </div>
  );
}

export default NotificationBanner;
