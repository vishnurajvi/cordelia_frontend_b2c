import React, { useContext, useState, useEffect, useRef } from "react";
import styles from "./TravelItineraryCard.module.scss";
import { Row, Col, Card, Button } from "antd";
import { EnvironmentFilled, StarFilled } from "@ant-design/icons";
import CardImg from "../../assets/img/cruise-routes/card.png";
import RouteMap from "../../assets/img/cruise-routes/route_map.png";
import Modal from "../Modal";
const { Meta } = Card;

function TravelItineraryCard(props) {
  // const {title, cruise_name, ports, starting_price, dates, route_img, explore_link, offer_description } = props;
  const [modal, setModal] = useState(false);
  const title = "Europe Street beat";
  const cruise_name = "Lorem Ipsum";
  const ports = [
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
  ];
  const starting_price = "₹27,000";
  const dates = [
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Lorem Ipsum",
  ];
  const route_img =
    "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png";
  const explore_link = "Lorem Ipsum";
  const offer_description = "Lorem Ipsum";

  const modalToggle = () => {
    setModal(!modal)
  }

  return (
        <div className="bg-white rounded-lg shadow-2xl m-5 max-h-86">
            <div className="grid grid-cols-1 lg:grid-cols-3">
              <div className="col-span-1">
              <img className="lg:rounded-l-lg w-full h-full object-cover " alt="Travel Image" src={CardImg}></img>
              </div>
              <div className="col-span-2">
                  <div className="grid grid-cols-1 lg:grid-cols-3 p-8">
                    <div className="col-span-2">
                      <h3 className="text-3xl">3 Night Cruises</h3>
                      <h4 className="text-2xl text-j-gray">Kerala Peace Cruise</h4>
                      <div className="grid grid-cols-2 lg:grid-rows-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 xl:gap-2 grid-flow-row mr-3 my-8">
                          <div className="mr-3 col-span-full lg:col-span-1 lg:row-span-3">
                              PORTS:
                          </div>
                          <span ><i class="fas fa-map-marker-alt mr-3"></i>Mumbai</span>
                          <span ><i class="fas fa-map-marker-alt mr-3"></i>Chennai</span>
                          <span ><i class="fas fa-map-marker-alt mr-3"></i>Cochin</span>
                          <span ><i class="fas fa-map-marker-alt mr-3"></i>Lakshadweep</span>
                          <span ><i class="fas fa-map-marker-alt mr-3"></i>Trincomalee</span>
                          <span ><a className="text-j-orange underline italic" onClick={modalToggle} >Route Map</a></span>
                      </div>
                      <h3  className="text-lg text-j-gray">CRUISE DATES</h3>
                      <div className="grid grid-rows-2 grid-cols-2 gap-4 grid-flow-row lg:w-2/3 mb-5">
                        <Button className="bg-j-magenta">Sat, 21 Nov 2021</Button>
                        <Button className="bg-j-magenta">Sat, 21 Nov 2021</Button>
                        <Button className="bg-j-magenta">Sat, 21 Nov 2021</Button>
                        <Button className="bg-j-magenta">Sat, 21 Nov 2021</Button>
                      </div>
                      <div><a className="text-j-orange underline italic" >View all dates</a></div>
                    </div>
                    <div className="col-span-1">
                      <p className="text-j-gray"><i class="fas fa-map-marker-alt mr-3"></i>2 offers available on this itinerary</p>
                      <p>STARTING From</p>
                      <p><strong className="text-3xl">₹27,000</strong>/person</p>
                      <p>+ Insurance, Taxes & Fees</p>
                      <a className="text-j-orange underline italic">View inclusions</a>
                      <div className="grid gap-4 grid-cols-2 my-5">
                        <Button className="bg-j-magenta uppercase">Explore Itinerary</Button>
                        <Button className="bg-j-magenta uppercase">Book now</Button>
                        </div>
                      
                    </div>
                  </div>
              </div>
            </div>
            <Modal show={modal} onClose={modalToggle}>
              <div>
                <img className="object-cover w-full" alt="Travel Image" src={RouteMap}></img>
              </div>
            </Modal>
        </div>
    // <div className="TravelItineraryCard">
    //   <Card
    //     style={{
    //       border: "none",
    //       borderRadius: "10px",
    //       WebkitBoxShadow: "-2px 17px 5px -7px rgba(0,0,0,0.12)",
    //       MozBoxShadow: "-2px 17px 5px -7px rgba(0,0,0,0.12)",
    //       boxShadow: "-2px 17px 5px -7px rgba(0,0,0,0.12)",
    //     }}
    //   >
    //     <div className={styles.container}>
    //       <img alt="Travel Image" src={route_img}></img>
    //       <div style={{ padding: "24px" }}>
    //         <Row>
    //           <Col span={18}>
    //             <Meta title={title} description={cruise_name} />
    //             <div style={{ padding: "24px 0 0 0" }}>
    //               <ul id="port" className={styles.travelPortsList}>
    //                 Ports:
    //                 {ports.map((port, i) => (
    //                   <li
    //                     key={i}
    //                     className={
    //                       i === ports.length - 1
    //                         ? styles.travelPortsListLastItem
    //                         : styles.travelPortsListItem
    //                     }
    //                   >
    //                     <span className={styles.travelPortsListSpan}>
    //                       <EnvironmentFilled /> {port}
    //                     </span>
    //                   </li>
    //                 ))}
    //                 <a>Route Map</a>
    //               </ul>
    //             </div>
    //             <div
    //               style={{ padding: "12px 0 0 0", textTransform: "uppercase" }}
    //             >
    //               <div>Cruise Dates</div>
    //               <div>
    //                 {dates.map((date, i) => (
    //                   <Button
    //                     key={i}
    //                     className={
    //                       i === 0
    //                         ? styles.travelDateFirstItem
    //                         : styles.travelDateItems
    //                     }
    //                   >
    //                     {date}
    //                   </Button>
    //                 ))}
    //               </div>
    //               <a>View All Dates</a>
    //             </div>
    //           </Col>
    //           <Col span={6}>
    //             <div style={{ color: "#616161" }}>
    //               <StarFilled spin /> 2 offers available on this itinerary
    //             </div>
    //             <div style={{ color: "#616161" }}>
    //               <div>
    //                 <p>Starting From</p>
    //                 <h1>
    //                   {starting_price}&nbsp;<span>/&nbsp;person</span>
    //                 </h1>
    //                 <p>+ Insurance, Taxes & Fees</p>
    //                 <a>View inclusions</a>
    //               </div>
    //             </div>
    //             <div style={{ display: "inline", color: "#616161" }}>
    //               <a>Explore Itinerary</a>
    //               <Button type="primary">Book Now</Button>
    //             </div>
    //           </Col>
    //         </Row>
    //       </div>
    //     </div>
    //   </Card>
    // </div>
  );
}

export default TravelItineraryCard;
