import React, { useState } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {
  Button,
  Carousel,
  Col,
  Dropdown,
  Form,
  Input,
  Menu,
  Row,
  Tag,
} from "antd";
import {
  FunnelPlotFilled,
  CloseOutlined,
  SortAscendingOutlined,
  SortDescendingOutlined,
} from "@ant-design/icons";
import styles from "./CommonCards.module.scss";
import TravelItinerary from "./TravelItineraryCard";
import Container from "../Container";

const { CheckableTag } = Tag;

export const CruiseRoutesCards = (props) => {
  const [selectedTags, setselectedTags] = useState(["2 - Nights"]);
  const tagsData = [
    "1 - Night",
    "2 - Nights",
    "3 - Nights",
    "4 - Nights",
    "5 - Nights",
    "6 - Nights",
    "7 - Nights",
  ];
  const handleChange = (tag, checked) => {
    const nextSelectedTags = checked
      ? [tag]
      : selectedTags.filter((t) => t !== tag);
    setselectedTags({ selectedTags: nextSelectedTags });
  };
  const menu = (
    <Menu>
      <Menu.Item key={1}>Price: Low to High <SortAscendingOutlined /></Menu.Item>
      <Menu.Item key={2}>Price: High to Low <SortDescendingOutlined /></Menu.Item>
      <Menu.Item key={3}>Number of Nights: Low to High <SortAscendingOutlined /></Menu.Item>
      <Menu.Item key={4}>Number of Nights: High to Low <SortDescendingOutlined /></Menu.Item>
    </Menu>
  );
  return (
    <div className={styles.FilterSectionBackground}>
      <Row gutter={16}>
        <Col className="gutter-row" span={16}>
          <div className={styles.FilterNightTags}>
            <span style={{ marginRight: 8 }}>
              <FunnelPlotFilled /> Filter By:
            </span>
            {tagsData.map((tag, i) => (
              <CheckableTag
                key={i}
                checked={selectedTags.indexOf(tag) > -1}
                onChange={(checked) => handleChange(tag, checked)}
                style={{ margin: "0 10px" }}
              >
                {selectedTags.indexOf(tag) > -1 ? (
                  <>
                    <CloseOutlined />
                    &nbsp;{tag}
                  </>
                ) : (
                  <>{tag}</>
                )}
              </CheckableTag>
            ))}
          </div>
        </Col>
        <Col className="gutter-row" span={8}>
          <div className={styles.FilterSortSection}>
            <Dropdown overlay={menu}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Sort Filter <SortAscendingOutlined />
              </a>
            </Dropdown>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export const TravelItineraryCards = (props) => {
  return (
    <Container>
      <TravelItinerary key="1"></TravelItinerary>
    </Container>
  );
};

CruiseRoutesCards.TravelItineraryCards = TravelItineraryCards;

export default CruiseRoutesCards;
