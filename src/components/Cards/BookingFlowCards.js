import { Row, Col, Button } from "antd";
import { ArrowLeftOutlined } from "@ant-design/icons";
import ProgressStepper from "../ProgressStepper/ProgressStepper";
import Card from "../UI/Card";
import classes from "./BookingFlowCards.module.scss";
import cx from "classnames";
import Container from "../Container";
// import KingBedOutlinedIcon from '@material-ui/icons/KingBedOutlined';

export const BookingFlowCards = () => {
  return <></>;
};

export const ProgressBar = (props) => {
  return (
      <Container>
        <div className="grid grid-cols-4 lg:grid-cols-12 gap-4 my-5">
          <div className="col-span-2 lg:col-span-2 order-1 m-1">
          <Button type="primary" >
            <ArrowLeftOutlined />
          </Button>
          </div>
          <div className="col-span-4 lg:col-span-8 order-3 lg:order-2">
          <ProgressStepper  />
          </div>
          <div className="col-span-2 lg:col-span-2 order-2 lg:order-3 text-right m-1">
          <Button type="primary">Modify Booking</Button>
          </div>
        </div>
      </Container>
  );
};

export const BookingSummary = (props) => {
  return (
    <Container>
      <h1 className={classes.heading}>Booking Summary</h1>
      <div className="grid grid-cols-1 lg:grid-cols-2">
        <div>
          <p className={classes.sectionHeading}>Booking Details</p>
          <BookingDetails />
        </div>
        <div>
          <p className={classes.sectionHeading}>Price Details</p>
          <PriceDetails />
        </div>
      </div>
    </Container>
    // <div className={`container ${classes.bookingSummary}`}>
    //   <div className={cx(classes.row)}>
    //     <div className="col-12">
    //       <h1 className={classes.heading}>Booking Summary</h1>
    //     </div>
    //   </div>
    //   <div className={cx(classes.row, "mt-3")}>
    //     <div className="col-md-6 col-12">
    //         <p className={classes.sectionHeading}>Booking Details</p>
    //         <BookingDetails/>
    //       </div>
    //       <div className="col-md-6 col-12">
    //         <p className={classes.sectionHeading}>Price Details</p>
    //         <PriceDetails/>
    //       </div>
    //   </div>
    // </div>
  );
};

export const BookingDetails = (props) => {
  return (
    <Card className={cx(classes.bookingDetail)}>
      <div className={classes.cardContent}>
        <div className="ship-detail" style={{ marginBottom: "20px" }}>
          <p className={`mb-0 ${classes.secondaryText}`}>Ship</p>
          <h5 className={`font-weight-bold ${classes.primaryText}`}>Empress</h5>
        </div>
        <div className={cx("row")} style={{ marginBottom: "20px" }}>
          <div className={cx("col-6")}>
            <div className={cx(classes.secondaryText)}>Departure</div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              Goa, India
            </div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              Sun, May 23, 2021 4:00PM
            </div>
          </div>
          <div className={cx("col-6")}>
            <div className={cx(classes.secondaryText)}>Arrival</div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              Goa, India
            </div>
            <div className={cx(classes.primaryText, "font-weight-bold")}>
              Tue, May 25, 2021 12:00PM
            </div>
          </div>
        </div>
        <div
          className={cx(classes.cabinDetails)}
          style={{ marginBottom: "20px" }}
        >
          <div className={cx("row")} style={{ marginBottom: "8px" }}>
            <div className={cx("col-1 ")}>
              {/* <KingBedOutlinedIcon style={{color:"#EA725B"}}/> */}
            </div>
            <div className={cx("col-10")}>
              <div className={cx(classes.secondaryText)}>Cabin 1</div>
            </div>
          </div>
          <div className={cx("row")}>
            <div className={cx("col-1")}></div>
            <div
              className={cx("col-10", classes.primaryText, "font-weight-bold")}
            >
              <div>Interior</div>
              <div> Deck No: 9</div>
              <div>Room No: 9169</div>
              <div>1 Guest</div>
            </div>
          </div>
        </div>
        <div>
          <a href="#">Cancellation and Reschedule policy</a>
        </div>
      </div>
    </Card>
  );
};

export const PriceDetails = (props) => {
  return (
    <Card className={`${classes.priceDetail}`}>
      <div className={classes.cardContent}>
        <div className={`${classes.section}`}>
          <div className={classes.heading}>
            <div className={classes.sectionRow}>
              <span>Adult</span>
              <span>&#8377;18024</span>
            </div>
          </div>
          <div className={classes.subHeading}>
            <div className={classes.sectionRow}>
              <span>Cabin Fare:</span>
              <span>&#8377;1800</span>
            </div>
            <div className={classes.sectionRow}>
              <span>Port Charge:</span>
              <span>&#8377;2034</span>
            </div>
            <div className={classes.sectionRow}>
              <span>Gratuity:</span>
              <span>&#8377;2314</span>
            </div>
          </div>
        </div>

        <div className={`${classes.section}`}>
          <div className={classes.heading}>
            <div className={classes.sectionRow}>
              <span>Promo Code</span>
              <span>&#8377;0</span>
            </div>
          </div>
          <div className={classes.subHeading}>
            <div className={classes.sectionRow}>
              <span>Cabin Fare:</span>
              <span>&#8377;0</span>
            </div>
          </div>
        </div>

        <div className={`${classes.section}`}>
          <div className={classes.heading}>
            <div className={classes.sectionRow}>
              <span>Sub Total</span>
              <span>&#8377;18024</span>
            </div>
          </div>
          <div className={classes.subHeading}>
            <div className={classes.sectionRow}>
              <span>Cabin Fare:</span>
              <span>&#8377;18024</span>
            </div>
          </div>
        </div>

        <div className={`${classes.section}`}>
          <div className={classes.heading}>
            <div className={classes.sectionRow}>
              <span>Taxes</span>
              <span>&#8377;3244</span>
            </div>
          </div>
          <div className={classes.subHeading}>
            <div className={classes.sectionRow}>
              <span>GST:</span>
              <span>&#8377;3244</span>
            </div>
          </div>
        </div>
      </div>
      <div className={`${classes.cardFooter}`}>
        <div>
          <span>Grand Total</span>
        </div>
        <div>
          <span>&#8377;123039</span>
        </div>
      </div>
    </Card>
  );
};

BookingFlowCards.ProgressBar = ProgressBar;
BookingFlowCards.BookingSummary = BookingSummary;
export default BookingFlowCards;
