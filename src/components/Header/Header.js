import styles from "./Header.module.scss";
import Logo from "../../assets/img/logo.png";
import Banner from "../../assets/img/banner.png";
function Header() {
  return (
    <div className="Header">
      <header className={styles.siteHeader}>
        <div className={styles.siteIdentity}>
          <a href="#">
            <img src={Logo} alt="Site Name" />
          </a>
        </div>
        <nav className={styles.siteNavigation}>
          <ul className={styles.nav}>
            <li>
              <a href="/cruise-routes">Find a cruise</a>
            </li>
            <li>
              <a href="#">Destinations</a>
            </li>
            <li>
              <a href="#">My Bookings</a>
            </li>
            <li>
              <a href="#">Blog</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li>
          </ul>
        </nav>
      </header>
      {/* <img src={Banner} alt="Site Name" /> */}
    </div>
  );
}

export default Header;
